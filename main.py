import json
import os
from datetime import datetime
from http import HTTPStatus
from uuid import uuid4

import boto3
from aws_lambda_powertools.event_handler.api_gateway import (
    APIGatewayRestResolver, CORSConfig, Response, content_types)
from aws_lambda_powertools.logging import Logger, correlation_paths
from aws_lambda_powertools.tracing import Tracer
from boto3.dynamodb.conditions import Key

logger = Logger()
tracer = Tracer()


def __get_table_client():
    table_name = os.getenv("DDB_TABLE_NAME")
    dynamodb = boto3.resource("dynamodb")
    table = dynamodb.Table(table_name)
    return table


table = __get_table_client()

cors_config = CORSConfig(allow_origin="*")
app = APIGatewayRestResolver(cors=cors_config)


@app.exception_handler(ValueError)
def handle_value_error(ex: ValueError):
    metadata = {"path": app.current_event.path}
    logger.error(f"Malformed request: {ex}", extra=metadata)

    return Response(
        status_code=400,
        content_type=content_types.TEXT_PLAIN,
        body="Invalid request",
    )


@app.post("/appointment")
def create_appolintment():
    data = app.current_event.json_body
    current_time = datetime.utcnow().isoformat()[:-3] + "Z"

    try:
        table.update_item(
            Key={
                "PK": "APPOINTMENT",
                "SK": f"CAREGIVER#{data.get('caregiver_id')}#PATIENT#{data.get('patient_id')}",
            },
            UpdateExpression="""
                SET
                    caregiver_id = :caregiver_id,
                    patient_id = :patient_id,
                    meeting_link = :meeting_link,
                    meeting_time = :meeting_time,
                    updated_at = :updated_at
            """,
            ExpressionAttributeValues={
                ":caregiver_id": data.get("caregiver_id"),
                ":patient_id": data.get("patient_id"),
                ":meeting_link": data.get("meeting_link"),
                ":meeting_time": data.get("meeting_time"),
                ":updated_at": current_time,
            },
            ReturnValues="UPDATED_NEW",
        )
    except Exception:
        raise ValueError("Something went wrong")

    return Response(
        status_code=HTTPStatus.CREATED.value,
        content_type="application/json",
        body=HTTPStatus.CREATED.phrase,
    )


# An user will create a request for a meeting.
# It can be both from patient to caregiver and caregiver to patient
@app.post("/appointment/request/<user_id>")
def request_appointment(user_id):
    data = app.current_event.json_body
    current_time = datetime.utcnow().isoformat()[:-3] + "Z"

    item = {
        "PK": f"USER#{data.get('request_to')}",
        "SK": f"APPOINTMENT_REQUEST#{uuid4()}",
        "request_from": user_id,
        "request_to": data.get("request_to"),
        "proposed_time": data.get("proposed_time"),
        "created_at": current_time,
    }

    try:
        table.put_item(Item=item)
    except Exception:
        raise ValueError("Something went wrong")

    return Response(
        status_code=HTTPStatus.CREATED.value,
        content_type="application/json",
        body=HTTPStatus.CREATED.phrase,
    )


@app.get("/appointment/request/<user_id>")
def appointment_request_list(user_id):
    # query all record bellongs to that user
    response = table.query(
        KeyConditionExpression=Key("PK").eq(f"USER#{user_id}")
        & Key("SK").begins_with("APPOINTMENT_REQUEST")
    )

    # return all the data in a dict
    return Response(
        status_code=HTTPStatus.OK.value,
        content_type="application/json",
        body=json.dumps(response["Items"]),
    )


# request will be deleted after fulfillment or rejecvted
@app.delete("/appointment/request/<user_id>/<request_id>")
def delete_request(user_id, request_id):
    try:
        table.delete_item(
            Key={
                "PK": f"USER#{user_id}",
                "SK": f"APPOINTMENT_REQUEST#{request_id}",
            }
        )
    except Exception:
        raise ValueError("Something went wrong")

    return Response(
        status_code=HTTPStatus.OK.value,
        content_type="application/json",
        body=HTTPStatus.OK.phrase,
    )


@app.get("/appointment/caregiver/<caregiver_id>")
def get_caregiver_appointment_list(caregiver_id):
    # query all record bellongs to that user
    response = table.query(
        KeyConditionExpression=Key("PK").eq("APPOINTMENT")
        & Key("SK").begins_with(f"CAREGIVER#{caregiver_id}")
    )

    # return all the data in a dict
    return Response(
        status_code=HTTPStatus.OK.value,
        content_type="application/json",
        body=json.dumps(response["Items"]),
    )


@app.get("/appointment/patient/<patient_id>")
def get_patient_appointment_list(patient_id):
    # query all record bellongs to that user
    response = table.query(
        KeyConditionExpression=Key("PK").eq("APPOINTMENT"),
        FilterExpression="patient_id = :patient_id",
        ExpressionAttributeValues={":patient_id": patient_id},
    )

    # return all the data in a dict
    return Response(
        status_code=HTTPStatus.OK.value,
        content_type="application/json",
        body=json.dumps(response["Items"]),
    )


@logger.inject_lambda_context(correlation_id_path=correlation_paths.API_GATEWAY_REST)
def lambda_handler(event, context):
    # if(event["identity"] is None):
    #     return Response(
    #         status_code=HTTPStatus.UNAUTHORIZED.value,
    #         body=json.dumps({
    #             "status": HTTPStatus.UNAUTHORIZED.phrase,
    #             "message": HTTPStatus.UNAUTHORIZED.description
    #         }),
    #         content_type="application/json"
    #     )

    return app.resolve(event, context)
